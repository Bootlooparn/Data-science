import tensorflow
import keras
import theano
import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder, OneHotEncoder, StandardScaler
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import confusion_matrix
from sklearn.grid_search import GridSearchCV
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier

dataset = pd.read_csv('Churn_Modelling.csv')

x = dataset.iloc[:, 3:len(dataset.iloc[1])-1].values
y = dataset.iloc[:, len(dataset.iloc[1])-1].values

le = LabelEncoder()
x[:, 1] = le.fit_transform(x[:, 1])
le = LabelEncoder()
x[:, 2] = le.fit_transform(x[:, 2])

ohe = OneHotEncoder(categorical_features = [1])
x = ohe.fit_transform(x).toarray()
x = x[:, 1:]

sc = StandardScaler()
xTrain, xTest, yTrain, yTest = train_test_split(x, y, test_size = 0.2, random_state = 0)
xTrain = sc.fit_transform(xTrain)
xTest = sc.transform(xTest)

def getClassifier(optimizer):
    classifier = Sequential()
    layer1 = Dense(units = 6, kernel_initializer = 'uniform', activation = 'relu', input_dim = 11)
    layer2 = Dense(units = 6, kernel_initializer = 'uniform', activation = 'relu')
    layer3 = Dense(units = 1, kernel_initializer = 'uniform', activation = 'sigmoid')
    
    classifier.add(layer1)
    classifier.add(layer2)
    classifier.add(layer3)
    classifier.compile(optimizer= optimizer, loss = 'binary_crossentropy', metrics = ['accuracy'])
    return classifier

#Initial evaluation
#classifier = KerasClassifier(build_fn= getClassifier,batch_size = 10, epochs= 100)
#accuracies = cross_val_score(estimator=classifier, X=xTrain, y=yTrain, cv=10, n_jobs=-1)
#mean = accuracies.mean()
#variance = accuracies.std()

# Tuning
classifier = KerasClassifier(build_fn=getClassifier, epochs=100)
parameters = {
        'batch_size': [10, 25],
        'optimizer': ['adam', 'rmsprop']
        }
gridSearch = GridSearchCV(estimator = classifier,
                           param_grid = parameters,
                           scoring = 'accuracy',
                           cv = 10)
gridSearch = gridSearch.fit(xTrain, yTrain)
bestParam = gridSearch.best_params_
bestAcc = gridSearch.best_score_